package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Crie um programa que lê os clientes presentes no arquivo contas.csv e 
 * filtre aqueles que possuem o saldo superior à 7000.
 * Desses clientes, criar outro arquivo de texto que possua todos os clientes
 * no seguinte formato:
 * 
 * João da Silva<joaosilva@teste.com>, Maria da Penha<maria@teste.com>
 * 
 */
public class ListaEmails {
	public static void main(String[] args) throws IOException {
		Scanner scanner = new Scanner(System.in);
		Path pathOrigem = Paths.get("bin/contas.csv");
		Path pathDestino = Paths.get("bin/contasFiltradas.txt");
		
		ArrayList<String> itensArquivo = (ArrayList<String>) Files.readAllLines(pathOrigem);
		
		StringBuilder itensFiltrados = filtrarContas(itensArquivo);
		
		salvarArquivo(itensFiltrados, pathDestino);
		
		System.out.println(itensFiltrados);
		
	}
	
	public static StringBuilder filtrarContas(ArrayList<String> itensArquivo) {
		StringBuilder itensFiltrados = new StringBuilder();
		for(int i=0; i<itensArquivo.size(); i++) {
			String[] itens = itensArquivo.get(i).split(",");
			
			if(isInteger(itens[4]) && Integer.parseInt(itens[4]) >= 7000) {
				itensFiltrados.append(String.format("%s %s<%s>, ", itens[1], itens[2], itens[3]));
			}
		}
		return itensFiltrados;
	}
	
	public static void salvarArquivo(StringBuilder itensFiltrados, Path pathDestino) {
		
		try {
			byte[] listaBytes = itensFiltrados.toString().getBytes();
			Files.write(pathDestino, listaBytes);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean isInteger(String string) {
	    try {
	        Integer.valueOf(string);
	        return true;
	    } catch (NumberFormatException e) {
	        return false;
	    }
	}
}

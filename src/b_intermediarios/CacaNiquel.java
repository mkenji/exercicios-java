package b_intermediarios;

import java.util.Random;

/**
 * Crie um programa que simula o funcionamento de um caça níquel, da seguinte forma:
 * 
 * 1- Sorteie 3x um valor dentre as strings do vetor de valores
 * 2- Compare os valores sorteados e determine os pontos obtidos usando o
 * seguinte critério:
 * 
 * Caso hajam três valores diferentes: 0 pontos
 * Caso hajam dois valores iguais: 100 pontos
 * Caso hajam três valores iguais: 1000 pontos
 * Caso hajam três valores "7": 5000 pontos
 * 
 */
public class CacaNiquel {
	static int numCasas = 3;
	static String[] valores = {"abacaxi", "framboesa", "banana", "pera", "7"};
	
	public static void main(String args[]) {
		
		String[] itensSorteados = new String[3];
		for(int i = 0 ; i < numCasas ; i++) {
			itensSorteados[i] = realizarSorteio();
		}
		
		int pontos = calcularPontuacao(itensSorteados);
		for(int i = 0 ; i < numCasas ; i++) {
			System.out.println("["+itensSorteados[i]+"]");
		}

		System.out.println("Voce ganhou "+pontos);
	}
	
	public static String realizarSorteio() {
		
		Random random = new Random();
		int sorteio = random.nextInt(5);
		return (valores[sorteio]);
	}
	
	public static int calcularPontuacao(String[] itensSorteados) {
		int acertos = 0;
		if(itensSorteados[0].equals("7") && itensSorteados[1].equals("7") && itensSorteados[2].equals("7")) {
			return 5000;
		}
		
		for(int j = 0 ; j < valores.length; j++) {
			for(int i = 0 ; i < numCasas ; i++) {
				if(valores[j].equals(itensSorteados[i])) {
					acertos += 1;
				}
			}	
		}
		
		if(acertos == 3)
			return 1000;
		if(acertos == 2)
			return 100;
		
		return 0;
		
	}
}

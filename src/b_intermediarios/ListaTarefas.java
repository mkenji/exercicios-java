package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.nio.file.Path;

/**
 * Crie um programa que recebe um todo list e a escreve em um arquivo txt.
 * 
 * A cada iteração, o sistema deve pedir uma  única tarefa e exibir a possibilidade
 * de encerrar o programa.
 * 
 * Quando o usuário encerrar o programa, deve-se gerar o arquivo txt com as tarefas
 * que foram inseridas.
 *
 */
public class ListaTarefas {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String menu = "";
		//String lista = "";
		StringBuilder lista = new StringBuilder();
		
		while(!menu.equals("0")) {
			System.out.println("Insira uma tarefa ou 0 para sair");
			menu = scanner.nextLine();
			
			if(!menu.equals("0")) {
				lista.append(menu + "\n");
				//lista += menu + "\n";
			}
		}
		
		Path path = Paths.get("bin/b_intermediarios/listaTarefas.txt");
				
		try {
			//Files.write(path, lista.getBytes());
			byte[] listaBytes = lista.toString().getBytes();
			Files.write(path, listaBytes);
			System.out.println(lista);
			System.out.println("Lista de tarefas salvas em bin/b_intermediarios/listaTarefas.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

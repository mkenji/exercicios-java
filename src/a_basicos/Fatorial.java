package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe um número do usuário
 * e imprime a fatorial desse mesmo número.
 * 
 */
public class Fatorial {
	private static Scanner scanner;

	public static void main(String args[]) {
		scanner = new Scanner(System.in);
		int entrada, fatorial = 1;
		
		
		System.out.println("Insira um numero:");
		entrada = scanner.nextInt();
		
	    for(int i=1; i <= entrada; i++) {
	    	fatorial = fatorial*i;
	    }
	    
	    System.out.println("O fatorial de "+ entrada+" eh: "+fatorial);
	}
}

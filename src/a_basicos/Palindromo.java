package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe uma palavra do terminal e determina
 * se ela é um palíndromo.
 * 
 * Exs: 
 * 
 * input: ovo
 * output: A palavra ovo é um palíndromo
 * 
 * input: jose
 * output: A palavra jose não é um palíndromo 
 */
public class Palindromo {
	private static Scanner scanner = new Scanner(System.in);
	
	public static boolean calculaPalindrome(String palavra) {
		float meioPalavra = palavra.length() /2;
		
		for(int i = 0; i < meioPalavra; i++) {
			if(palavra.charAt(i) != palavra.charAt(palavra.length()-1-i)) {
				return false;
			}
		}
		return true;
	}

	public static void main(String args[]) {
		
		String entrada = "";
		
		System.out.println("Insira uma palavra:");
		entrada = scanner.nextLine();
		
		if(calculaPalindrome(entrada)) {
			System.out.println("A palavra eh palindrome");
		}else {
			System.out.println("A palavra nao eh palindrome");
		}
	}
	
	
	
}

package a_basicos;

/**
 * Crie um programa que imprima todas as cartas do baralho.
 * 
 * Exemplo: 
 * 
 * Ás de Ouros
 * Ás de Espadas
 * Ás de Copas
 * Ás de Paus
 * Dois de Ouros
 * ...
 * 
 */
public class Baralho {
	public static void main(String[] args) {
		String[] numeros = {"As", "Dois", "Tres", "Quarto", "Cinco", "Seis", "Sete", "Oito", "Nove", "Dez", "Valete", "Damas", "Reis"};
		String[] siglas = {"Copa", "Paus", "Ouros", "Espadas"};
		
		for(int i=0; i< numeros.length; i++) {
			for(int j=0; j < siglas.length; j++) {
				System.out.println(numeros[i]+" de "+siglas[j]);
			}
		}
		
	}
}

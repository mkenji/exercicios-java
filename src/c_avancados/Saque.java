package c_avancados;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Crie um programa que faça um saque no arquivo contas.csv. O programa
 * deve receber um id e o valor do saque. Ele deve informar caso o id
 * não seja encontrado ou caso o valor não esteja disponível.
 * 
 * Após o saque, o arquivo deve ser atualizado.
 * 
 */
public class Saque {
	
	static HashMap<Integer, String> mapaContas = new HashMap<Integer, String>();
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		carregarContas(); 
		
		System.out.println("Insira o ID do cliente:");
		int idCliente = scanner.nextInt();
		if(!mapaContas.containsKey(idCliente)) {
			System.out.println("Conta nao existe");
			return;
		}
		
		System.out.println("Insira o valor a ser debitado: ");
		int valorDebito = scanner.nextInt();
		System.out.println(mapaContas.get(idCliente));
		if(debitarValor(idCliente, valorDebito)) {
			System.out.println("Valor debitado com sucesso!");
			System.out.println(mapaContas.get(idCliente));
			salvarArquivo();
		} else {
			System.out.println("Saldo insuficiente!");
		}
				
	}
	
	public static boolean carregarContas() {
		Path pathOrigem = Paths.get("bin/contas.csv");
		ArrayList<String> itensArquivo = null;
			
		try {
			itensArquivo = (ArrayList<String>) Files.readAllLines(pathOrigem);
			for(int i=1; i<itensArquivo.size(); i++) {
				String[] itens = itensArquivo.get(i).split(",");
				mapaContas.put(Integer.parseInt(itens[0]), itensArquivo.get(i));
			}	
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean debitarValor(int idCliente, int valorDebito) {
		String[] itens = mapaContas.get(idCliente).split(",");
		int valorAtual = Integer.parseInt(itens[4]);
		
		if(valorAtual > valorDebito) {
			int valorAtualizado = valorAtual - valorDebito;
			itens[4] = Integer.toString(valorAtualizado);
			String resultadoAtualizado = Arrays.toString(itens);
			
			mapaContas.replace(idCliente, resultadoAtualizado);
			return true;
		} 
		
		return false;
	}
	
	public static void salvarArquivo() {
		Path pathDestino = Paths.get("bin/contasAtualizada.csv");
			
		try {
			String itensAjustados = mapaContas.values().toString();
			System.out.println(itensAjustados);
			byte[] listaBytes = itensAjustados.getBytes();
			Files.write(pathDestino, listaBytes);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean isInteger(String string) {
	    try {
	        Integer.valueOf(string);
	        return true;
	    } catch (NumberFormatException e) {
	        return false;
	    }
	}
	
	
}
